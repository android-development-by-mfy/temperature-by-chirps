# Temperature By Chirps

This application only contains one page. In this page, it expects from user the number of chirps that a cricket makes in 25 secs and tells the temperature in Celcius accordingly. It uses information in this site: https://www.almanac.com/content/predict-temperature-cricket-chirps
