package com.mfy.temperaturebychirps;

import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText eT_numberOfChirps;
    Button b_calculate;
    TextView tV_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eT_numberOfChirps = findViewById(R.id.eT_numberOfChirps);
        b_calculate = findViewById(R.id.b_calculate);
        tV_result = findViewById(R.id.tV_result);

        b_calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eT_numberOfChirps.getText().toString().isEmpty()) {
                    Toast.makeText(MainActivity.this, getString(R.string.number_field_empty), Toast.LENGTH_SHORT).show();
                    tV_result.setText("");
                    return;
                }

                int numberOfChirps = Integer.parseInt(eT_numberOfChirps.getText().toString());
                double temperature = numberOfChirps / 3.0 + 4;
                tV_result.setText(String.format(getString(R.string.result), temperature));
            }
        });
    }
}
